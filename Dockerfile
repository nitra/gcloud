FROM google/cloud-sdk:alpine

# Docker install
RUN wget https://download.docker.com/linux/static/stable/x86_64/docker-19.03.8.tgz --output-document="/tmp/docker.tgz" \
    && tar -xz -C /tmp -f /tmp/docker.tgz \
    && rm /tmp/docker.tgz \
    && mv /tmp/docker/docker* /usr/local/bin/ 